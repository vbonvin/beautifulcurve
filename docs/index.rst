.. BeautifulCurve documentation master file, created by
   sphinx-quickstart on Fri Nov 16 07:03:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BeautifulCurve's documentation!
==========================================

Basic display of python data with matplotlib is simple. But it can also be quite ugly.

Advanced display of python data with matplotlib is not simple. And it can be ugly as well.

BeautifulCurve allows you to display your complex data sets in a fancy fashion, without the struggle of spending hours in the matplotlib documentation, and without having to write hundreds of line of code.

Sometimes, the beauty of the outside matters.

.. toctree::
   :maxdepth: 1

      Source code documentation <source/modules>
