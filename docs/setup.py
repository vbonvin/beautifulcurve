from distutils.core import setup

setup(
	name='BeautifulCurve',
	version='0.1',
	description='Tired of browsing though matplotlib doc everytime you want to make a plot that is not ugly? BeautifulCurve is for you!',
	packages=["bfcurve"]
)