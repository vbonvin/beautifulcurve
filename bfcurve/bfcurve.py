import sys
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import matplotlib.dates
import matplotlib.lines


class Curve:
	"""
	A class that transform correlated series in a Curve object, easier to manipulate.
	"""

	def __init__(self, xs, ys, xerrs=None, yerrs=None, name="A beautiful curve", plotcolor="crimson", ploterrorbars=True):

		"""
		Initiate the curve object.
		"""

		self.xs = np.array(xs)
		self.ys = np.array(ys)
		if xerrs is not None:
			self.xerrs = np.array(xerrs)
		else:
			self.xerrs = xerrs
		if xerrs is not None:
			self.yerrs = np.array(yerrs)
		else:
			self.yerrs = yerrs
		self.name = name
		self.plotcolor = plotcolor
		self.ploterrorbars = ploterrorbars


		# shifts in x and y are stored in these variables. We never apply shifts
		self.xshift = 0.0
		self.yshift = 0.0


		# display attribute, can be easily overriden
		self.markerstyle = "."
		self.markersize = 5
		self.style = "-"
		self.ecolor = "#777777"
		self.linewidth = 2.5
		self.elinewidth = 0.5


		self.test()



	## LIST OF INTERESTING VARIABLES I MIGHT DECIDE TO ADD LATER

	# self.mask = np.array([True, True, False, True, True])
	# self.labels = ["0", "1", "2 = Huge problem", "3", "4"]

	# self.ploterrorbars = True
	# """@type: boolean
	# @ivar: A flag to show or hide errorbars in plots of this lightcurve."""
	#
	# self.showlabels = False
	# """@type: boolean
	# @ivar: A flag to show or hide labels in plots of this lightcurve."""
	# # Values of the two obvious possible shifts.
	#
	# self.timeshift = 0.0
	# """@type: float
	# @ivar: A float giving the shift in time (same unit as jds) that was applied to the lightcurve.
	# This is updated at every shift, so that a lightcurve is always "aware" of its eventual shift.
	# """
	#
	# self.magshift = 0.0
	# """@type: float
	# @ivar: A float giving the shift in magnitude (similar to timeshift)."""
	#
	# self.fluxshift = 0.0
	# """@type: float
	# @ivar: A float giving the shift in flux (!). As we work with magnitudes, a shift in flux is a bit special.
	# Note by the way that a flux shift does not commute with a magnitude shift !
	# """

	def test(self):
		"""
		A quick series of assertion tests to make sure everything has the same length

		"""

		assert (len(self.xs) == len(self.ys)), "%s's xs and ys have a different length!" % self.name

		if self.xerrs is not None:
			assert (len(self.xs) == len(self.xerrs)), "%s's xs and xerrs have a different length!" % self.name
		if self.yerrs is not None:
			assert (len(self.ys) == len(self.yerrs)), "%s's ys and yerrs have a different length!" % self.name


	def shiftx(self, shift):
		self.xshift += float(shift)

	def shifty(self, shift):
		self.yshift += float(shift)

	def getxs(self):
		return self.xs + self.xshift

	def getys(self):
		return self.ys + self.yshift



def colorize(cslist, colors=None):
	"""
	Colourise the curves following a nice predefined pattern.

	todo: make sur the pattern is colorblind-friendly

	:param cslist: list of curves you want to colourise
	:param colors: list of matplotlib colours
	"""

	if colors==None:
		colors = ["crimson", "royalblue", "seagreen", "purple", "indianred", "gold"]

	for i, c in enumerate(cslist):
		c.plotcolor = colors[i % len(colors)]


def display(cslist=[],
			title=None, titlexpos=None, showlegend=True, xlabel=None, ylabel=None,
			text=None, xrange=None, yrange=None,
			legendloc="best", filename="screen", invertyaxis=False, subplot_style='rows', ax=None):


	# assertion tests
	assert subplot_style in ["rows", "columns"], "%s is not a known subplot_style. Use 'rows' or 'columns'." % subplot_style


	# start by defining a few styles elements
	if not isinstance(cslist[0], list):
		figsize = (10, 5)
	else:
		if subplot_style == 'rows':
			figsize = (10, 5*len(cslist))
		elif subplot_style == 'columns':
			figsize = (5*len(cslist), 5)
	#plotsize = (0.08, 0.97, 0.15, 0.95)
	capsize = 0
	xmintickstep = 10
	ymintickstep = 10
	transparent = False


	labelfontsize = 14
	titlefontsize = 18
	mpl.rcParams['font.family'] = 'serif'



	"""
	Now, the axes. We have either 
		i) no axis given, and cslist is a list of curves
		ii) no axis given, and cslist is a list of list of curves --> autosubplot
		iii) axis given, and cslist is a list of curve
		iv) axis given, and cslist is a list of list of curves --> error (for now...)
		
	todo: implement a way to pass a list of list of curves and a corrsponding number of axes  
	"""

	axeslist = []

	# case i) or iii)
	if not isinstance(cslist[0], list):
		if ax is None:
			fig = plt.figure(figsize=figsize)  # sets figure size
			#fig.subplots_adjust(left=plotsize[0], right=plotsize[1], bottom=plotsize[2], top=plotsize[3])
			axeslist.append(plt.gca())

		else:
			axeslist.append(ax)
		cslistlist = [cslist]

	# case ii) or iv)
	else:
		if ax is not None:
			raise AssertionError("ax should be None - let me handle the subplotting myself.")
		else:
			fig = plt.figure(figsize=figsize)
			if subplot_style == 'rows':
				#fig.subplots_adjust(left=plotsize[0], right=plotsize[1], bottom=plotsize[2], top=plotsize[3], hspace=0)
				fig.subplots_adjust(hspace=0)
			if subplot_style == 'columns':
				#fig.subplots_adjust(left=plotsize[0], right=plotsize[1], bottom=plotsize[2], top=plotsize[3], wspace=0)
				fig.subplots_adjust(wspace=0)

			for i in range(len(cslist)):
				if subplot_style == "rows":
					axeslist.append(plt.subplot(len(cslist), 1, i+1))
				elif subplot_style == "columns":
					axeslist.append(plt.subplot(1, len(cslist), i + 1))

		cslistlist = cslist

	nsubplots = len(cslistlist)
	for axind, (cslist, axes) in enumerate(zip(cslistlist, axeslist)):


		for curve in cslist:
			#todo: find a way to pass all the remaining curve display parameters as kwargs to errorbar
			axes.errorbar(x=curve.getxs(), y=curve.getys(), yerr=curve.yerrs, xerr=curve.xerrs, fmt=curve.style, marker=curve.markerstyle, markersize=curve.markersize, markeredgecolor=curve.plotcolor, color=curve.plotcolor, linewidth=curve.linewidth, ecolor=curve.ecolor, capsize=capsize, label=curve.name, elinewidth=curve.elinewidth)



			# We plot little round circles around masked points.
			# axes.plot(tmpjds[curve.mask == False], tmpmags[curve.mask == False], linestyle="None", marker="o", markersize=8., markeredgecolor="black", markerfacecolor="None", color="black")


			# # Labels if wanted :
			# if curve.showlabels:
			# 	for i, label in enumerate(curve.labels):
			# 		if label != "":
			# 			# axes.annotate(label, (curve.jds[i], curve.mags[i]))
			# 			if len(label) > 4:  # Probably jd labels, we write vertically :
			# 				axes.annotate(label, (tmpjds[i], tmpmags[i]), xytext=(-3, -70), textcoords='offset points',
			# 				              size=12, color=curve.plotcolor, rotation=90)
			# 			else:  # horizontal writing
			# 				axes.annotate(label, (tmpjds[i], tmpmags[i]), xytext=(7, -6), textcoords='offset points',
			# 				              size=12, color=curve.plotcolor)

		# if collapseref and len(reflevels) != 0:
		# 	print "WARNING : collapsing the refs %s" % (reflevels)
		# 	if not hidecollapseref:
		# 		axes.axhline(np.mean(np.array(reflevels)), color="gray", dashes=((3, 3)))  # the new ref



		if invertyaxis:
			#Something for astronomers only : we invert the y axis direction !
			axes.set_ylim(axes.get_ylim()[::-1])

		# if colourprop != None and hidecolourbar == False:
		# 	cbar = plt.colorbar(orientation='vertical', shrink=1.0, fraction=0.065, pad=0.025)
		# 	cbar.set_label(colournicename)


		# Single title, on the first subplot
		if not isinstance(title, list):
			if axind == 0:  # todo: improve the columns title placement
				if title is not None:
					if titlexpos is None:
						axes.annotate(title, xy=(0.5, 1.0), xycoords='axes fraction', xytext=(0, -4), textcoords='offset points', ha='center', va='top', fontsize=titlefontsize)
					else:
						axes.annotate(title, xy=(titlexpos, 1.0), xycoords='axes fraction', xytext=(0, -4), textcoords='offset points', ha='center', va='top', fontsize=titlefontsize)

		# one title per subplot
		else:
			if title is not None:
				if titlexpos is None:
					axes.annotate(title[axind], xy=(0.5, 1.0), xycoords='axes fraction', xytext=(0, -4), textcoords='offset points', ha='center', va='top', fontsize=titlefontsize)
				else:
					axes.annotate(title[axind], xy=(titlexpos, 1.0), xycoords='axes fraction', xytext=(0, -4), textcoords='offset points', ha='center', va='top', fontsize=titlefontsize)


		# The plot range, if given. We assume that we use the same plotrange for all subplots
		# todo: allow for custom ranges per subplots
		if xrange is not None:
			axes.set_xlim(xrange[0], xrange[1])

		if yrange is not None:
			# yrange can be either a single value, which will be interpreted as the scatter around the mean, or a list of values for a more traditional minmax range plot
			if type(yrange) == float or type(yrange) == int:
				# We find the mean y value of the stuff to plot :
				allys = []
				for curve in cslist:
					allys.extend(curve.ys)
				meanlevel = np.mean(np.array(allys))
				axes.set_ylim(meanlevel + yrange, meanlevel - yrange)
			else:
				axes.set_ylim(yrange[0], yrange[1])

		# The ticks values and labels.
		if subplot_style == 'rows' and axind == nsubplots-1 and xlabel is not None:
			axes.set_xlabel(xlabel, fontsize=labelfontsize)
		if subplot_style == 'rows' and axind < nsubplots-1:
			axes.set_xticklabels([])
		if subplot_style == 'columns' and xlabel is not None:
			axes.set_xlabel(xlabel, fontsize=labelfontsize)

		if subplot_style == 'columns' and axind == nsubplots-1 and ylabel is not None:
			axes.set_ylabel(ylabel, fontsize=labelfontsize)
		if subplot_style == 'columns' and axind > 0:
			axes.set_yticklabels([])
		if subplot_style == 'rows' and ylabel is not None:
			axes.set_ylabel(ylabel, fontsize=labelfontsize)


		if showlegend and axind == 0:
			axes.legend(loc=legendloc, numpoints=1, prop=fm.FontProperties(size=12))

		# if showdates:  # Be careful when you change something here, it could mess up the axes.
		# 	# Especially watch out when you change the plot range.
		# 	# This showdates stuff should come at the very end
		# 	minjd = axes.get_xlim()[0]
		# 	maxjd = axes.get_xlim()[1]
		# 	# axes.set_xlim(minjd, maxjd)
		# 	yearx = axes.twiny()
		# 	yearxmin = util.datetimefromjd(minjd + 2400000.5)
		# 	yearxmax = util.datetimefromjd(maxjd + 2400000.5)
		# 	yearx.set_xlim(yearxmin, yearxmax)
		# 	yearx.xaxis.set_minor_locator(matplotlib.dates.MonthLocator())
		# 	yearx.xaxis.set_major_locator(matplotlib.dates.YearLocator())
		# 	yearx.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%Y'))
		# 	yearx.xaxis.tick_top()
		# 	if keeponlygrid:
		# 		yearx.set_xticklabels([])


		# reformat axis locators
		minorxLocator = MultipleLocator(xmintickstep)
		axes.xaxis.set_minor_locator(minorxLocator)

		minoryLocator = MultipleLocator(ymintickstep)
		axes.yaxis.set_minor_locator(minoryLocator)


		# if showgrid:
		# 	axes.grid(zorder=20, linestyle='dotted')

		# if text != None and axind == 0:
		# 	for line in text:
		# 		axes.text(line[0], line[1], line[2], transform=axes.transAxes, **line[3])

		#todo: add the possibility to have an insert of flexible size, position and zoom options.
		# if showinsert:
		# 	assert insertname is not None
		# 	from matplotlib._png import read_png
		# 	from matplotlib.offsetbox import OffsetImage, AnnotationBbox
		# 	im = read_png(insertname)
		# 	imagebox = OffsetImage(im, zoom=0.5, interpolation="sinc", resample=True)
		# 	ab = AnnotationBbox(imagebox, xy=(1.0, 1.0), xycoords='axes fraction', xybox=(-75, -75),
		# 	                    boxcoords="offset points",
		# 	                    pad=0.0, frameon=False
		# 	                    )
		# 	axes.add_artist(ab)


	if ax is not None:
		return

	plt.tight_layout()
	if filename == "screen":
		plt.show()
	else:
		plt.savefig(filename, transparent=transparent)
		print("Plot written to %s" % filename)
		plt.close()


