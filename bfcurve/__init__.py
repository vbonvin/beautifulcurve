__all__ = ["bfcurve"]



# todo: find a way to use absolute imports instead, i.e. bfcurve.bfcurve instead of bfcurve

import os, sys, inspect
path = os.path.dirname(os.path.abspath(inspect.stack()[0][1]))
sys.path.append(path)
