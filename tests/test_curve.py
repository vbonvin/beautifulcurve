"""
Test script to check that nothing breaks when loading a curve and playing with it
"""

import os, sys
import numpy as np
import random
import copy
import matplotlib.pyplot as plt

path = os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), '../')
sys.path.append(path)

import bfcurve.bfcurve as bfcurve
import unittest


class TestCurve(unittest.TestCase):

	def setUp(self):
		# init new values at each test
		self.initvals()

	# def test_xshifts(self):
	# 	self.assertEqual(multiply(3, 4), 12)
	#
	# def test_yshifts(self):
	# 	self.assertEqual(multiply('a', 3), 'aaa')


	def initvals(self):
		# create a random curve
		xs = np.arange(100)
		ys = [random.randint(0, 100) for _ in range(100)]
		xerrs = [np.abs(random.normalvariate(0, 2)) for _ in range(100)]
		yerrs = [np.abs(random.normalvariate(0, 5)) for _ in range(100)]

		self.vals = (xs, ys, xerrs, yerrs)

	def test_initcurves(self):
		(xs, ys, xerrs, yerrs) = self.vals
		self.curve = bfcurve.Curve(xs=xs, ys=ys, name="a simple random curve")

		self.assertEqual(len(self.curve.getxs()), len(self.vals[0]))
		self.assertEqual(len(self.curve.getys()), len(self.vals[1]))

	def test_shifts(self):
		(xs, ys, xerrs, yerrs) = self.vals
		self.curve = bfcurve.Curve(xs=xs, ys=ys, name="a simple random curve")
		self.curve.shiftx(-10)
		self.curve.shifty(+10)
		self.assertEqual(self.curve.getxs()[0], -10)


	def test_display(self):
		(xs, ys, xerrs, yerrs) = self.vals
		self.curve = bfcurve.Curve(xs=xs, ys=ys, name="a simple random curve")
		self.scatter = bfcurve.Curve(xs=xs, ys=ys, xerrs=xerrs, yerrs=yerrs, name='a simple random scatter plot')
		self.scatter.style = "."

		bfcurve.colorize([self.curve, self.scatter])

		# simple display
		bfcurve.display([self.curve, self.scatter], title="A random beautiful plot")

		# combined rows display
		bfcurve.display([[self.curve], [self.scatter]], subplot_style="rows", title=["a beautiful curve", "a beautiful scatter plot"], titlexpos=0.4, xlabel="Xs", ylabel="Ys")

		# combined columns display
		bfcurve.display([[self.curve], [self.scatter]], subplot_style="columns", title=["a beautiful curve", "a beautiful scatter plot"], xlabel="Xs", ylabel="Ys")


		# passing an axes as argument
		fig = plt.figure()
		axes = plt.subplot(111)
		bfcurve.display([self.curve, self.scatter], ax=axes, invertyaxis=True)
		plt.show()

		# failing display
		self.assertRaises(AssertionError, bfcurve.display, cslist=[[self.curve], [self.scatter]], subplot_style="rows", ax=axes)

		# changing the display range
		bfcurve.display([self.curve, self.scatter], xrange=[random.randint(10, 20), random.randint(70, 90)], yrange=[random.randint(10, 20), random.randint(70, 90)])

		# other kw tests and save result
		bfcurve.display([self.curve, self.scatter], title="A random beautiful plot", invertyaxis=True, titlexpos=0.7, showlegend=False, yrange=35, xlabel="Xs", ylabel="Ys", filename='testplot.png')

	def tearDown(self):
		pass


# display a double
#bfcurve.display([[curve], [scatter]], subplot_style="rows", xlabel="x", ylabel="y")



if __name__ == '__main__':
	unittest.main()
